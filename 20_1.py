from pathlib import Path
from pprint import pprint
from collections import deque, defaultdict

path = Path(__file__).parent / "../input/20"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()


# make a graph like dictionary for neighbours
graph = {}
# make a dict for modules and their "states"
modules = {}

for line in lines:
    module_type, nei_str = line[0], line[1:]
    root, neis = nei_str.split(" -> ")
    nei_list = neis.split(", ")

    graph.update({root: nei_list})

    if module_type == "%":
        modules.update({root: 0})
    elif module_type == "&":
        modules.update({root: {}})  # dict of roots connected and signals sent


# update conjunction modules
for root, neis in graph.items():
    for nei in neis:
        if nei in modules.keys() and isinstance(modules[nei], dict):
            modules[nei].update({root: 0})


def cal_pulse(root: str, pulse: int, is_conj: bool, modules) -> int:
    if is_conj:
        for flow in modules[root].values():
            if flow == 0:
                return 1
        return 0

    if pulse == 1:
        return -1

    if modules[root] == 1:
        modules[root] = 0
        return 0
    elif modules[root] == 0:
        modules[root] = 1
        return 1
    else:
        print("skill issues")
        return -1


def iteration(curr_modules: dict):
    signal_q = deque([("roadcaster", 0)])

    low_count = 1
    high_count = 0
    while signal_q:
        root, pulse = signal_q.popleft()
        if root not in graph.keys():
            continue

        for nei in graph[root]:
            high_count += int(pulse == 1)
            low_count += int(pulse == 0)

            # update pulse for neighbours in conj module
            if nei in curr_modules.keys() and isinstance(curr_modules[nei], dict):
                curr_modules[nei][root] = pulse

            if nei in curr_modules.keys():
                next_pulse = cal_pulse(
                    nei, pulse, isinstance(curr_modules[nei], dict), curr_modules
                )

            if next_pulse != -1:
                signal_q.append((nei, next_pulse))

    return low_count, high_count


init_modules = modules.copy()

total_low, total_high = 0, 0
for i in range(1000):
    low_count, high_count = iteration(modules)
    total_low += low_count
    total_high += high_count

print("The final product of low and high pulses are:", total_low * total_high)
