from pathlib import Path

path = Path(__file__).parent / "../input/3"

# open the input file
with open(path, "r") as file:
    file_contents = file.read()


# method for detecting symbols
def issym(c: chr) -> bool:
    if c.isdigit() or c == ".":
        return False
    return True


# open the input file

# make a grid out of input
grid = [list(line) for line in file_contents.splitlines()]

n = len(grid)
m = len(grid[0])

num_list = []
for i in range(n):
    number = ""
    for j in range(m):
        if grid[i][j].isdigit():
            if number != "":
                number += grid[i][j]
            elif (
                # adjacent
                i > 0
                and issym(grid[i - 1][j])
                or i < n - 1
                and issym(grid[i + 1][j])
                or j < m - 1
                and issym(grid[i][j + 1])
                or j > 0
                and issym(grid[i][j - 1])
                or
                # diagonal
                i > 0
                and j > 0
                and issym(grid[i - 1][j - 1])
                or i > 0
                and j < n - 1
                and issym(grid[i - 1][j + 1])
                or i < n - 1
                and j > 0
                and issym(grid[i + 1][j - 1])
                or i < n - 1
                and j < m - 1
                and issym(grid[i + 1][j + 1])
            ):
                number = grid[i][j]

                # add previous digits
                k = 1
                while j - k >= 0 and grid[i][j - k].isdigit():
                    number = grid[i][j - k] + number
                    k += 1

        elif number != "":
            num_list.append(int(number))
            number = ""

    # edge case
    if number:
        num_list.append(int(number))

print("The sum of all part numbers in the schematic is: ", sum(num_list))
