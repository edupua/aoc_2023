# ONe of the fastest solutions in aoc_2023: uses manacher's algorithm

from email.policy import default
from pathlib import Path
from pprint import pprint

path = Path(__file__).parent / "../input/13"

# open the input file
with open(path, "r") as file:
    blocks = file.read().split("\n\n")


def rec(block):
    n = len(block)
    p = {}  # p stores the max palindrome lengths at each center
    center, border = (0, 1), 1

    for index in range(n - 1):
        i = (index, index + 1)
        p.update({i: 0})
        avg = index + 0.5
        mirror = tuple(sorted((2 * center[1] - i[1], 2 * center[0] - i[0])))

        if avg < border and mirror in p.keys():
            p[i] = min(int(border - avg), p[mirror])

        # now expand from currrent limit
        while (
            i[1] + p[i] < n
            and i[0] - p[i] >= 0
            and block[i[1] + p[i]] == block[i[0] - p[i]]
        ):
            p[i] += 1

        border = max(i[1] + p[i], border)
        if border == i[1] + p[i]:
            center = i

    valid_centers = [
        key[1]
        for key, value in p.items()
        if value != 0 and (key[0] == value - 1 or n - key[1] == value)
    ]
    return valid_centers[0] if valid_centers else 0


""" exactly one difference between tuples returns True else False """


def diff_is_one(a, b):
    flag = False
    for i in range(len(a)):
        if a[i] != b[i]:
            if flag:
                return False
            flag = True
    return True


def find_block(block):
    n = len(block)
    p = {}  # p stores the max palindrome lengths at each center
    center, border = (0, 1), 1

    for index in range(n - 1):
        i = (index, index + 1)
        p.update({i: 0})
        avg = index + 0.5
        mirror = tuple(sorted((2 * center[1] - i[1], 2 * center[0] - i[0])))

        if avg < border and mirror in p.keys():
            p[i] = min(int(border - avg), p[mirror])

        # now expand from currrent limit
        while (
            i[1] + p[i] < n
            and i[0] - p[i] >= 0
            and block[i[1] + p[i]] == block[i[0] - p[i]]
        ):
            p[i] += 1

        if (
            i[1] + p[i] < n
            and i[0] - p[i] >= 0
            and diff_is_one(block[i[1] + p[i]], block[i[0] - p[i]])
        ):
            possible_block = block.copy()
            possible_block[i[1] + p[i]] = possible_block[i[0] - p[i]]

            # make sure this possiblity is actually different and useful
            if rec(possible_block) != 0 and (
                rec(possible_block) != rec(block)
                or rec(list(reversed(possible_block))) != n - rec(block)
            ):
                return possible_block

        border = max(i[1] + p[i], border)
        if border == i[1] + p[i]:
            center = i

    return False


summary = 0
for block in blocks:
    hblock = [tuple(line) for line in block.splitlines()]
    vblock = [*zip(*hblock)]
    n = len(hblock)
    m = len(hblock[0])

    # try finding a new block with changes
    # smudge affects row
    new_block = find_block(hblock)
    if new_block:
        # priority issues
        if rec(new_block) == rec(hblock):
            new_block.reverse()
            summary += (n - rec(new_block)) * 100
        else:
            summary += rec(new_block) * 100
        continue

    new_block = find_block(vblock)
    # smudge affects column
    if new_block:
        # priority issues
        if rec(new_block) == rec(vblock):
            new_block.reverse()
            summary += m - rec(new_block)
        else:
            summary += rec(new_block)

print("The sum of all the mirror counts is: ", summary)
