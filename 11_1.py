from pathlib import Path
from pprint import pprint

path = Path(__file__).parent / "../input/11"

# open the input file
with open(path, "r") as file:
    rows = file.read().splitlines()

grid = list(list(row) for row in rows)


# double rows
i = 0
while i < len(grid):
    if grid[i].count("#") == 0:
        grid.insert(i, grid[i])
        i += 1
    i += 1


# double columns
j = 0
while j < len(grid[0]):
    column = [grid[i][j] for i in range(len(grid))]
    if column.count("#") == 0:
        new_grid = []
        for i in range(len(grid)):
            row = grid[i].copy()
            row.insert(j, ".")
            new_grid.append(row)
        j += 1
        grid = new_grid

    j += 1

# assign numbers and store locations
locations = {}
n = len(grid)
m = len(grid[0])
count = 1
for i in range(n):
    for j in range(m):
        if grid[i][j] == "#":
            grid[i][j] = count
            locations.update({count: (i, j)})
            count += 1
        if grid[i][j] == ".":
            grid[i][j] = 0

# make a dictionary for shortest distances:
dist = {}

shift = [(-1, 0), (0, -1), (0, 1), (1, 0)]


def bfs(source):
    s_i, s_j = locations[source]
    l = 0
    vis = set()
    layer = [(s_i, s_j)]

    while layer:
        new_layer = []
        for i, j in layer:
            if grid[i][j] != 0 and grid[i][j] > source:
                dist.update({(source, grid[i][j]): l})

            nei = [
                (i + di, j + dj)
                for di, dj in shift
                if 0 <= i + di < n and 0 <= j + dj < m
            ]

            for I, J in nei:
                if (I, J) not in vis:
                    vis.add((I, J))
                    new_layer.append((I, J))

        l += 1
        layer = new_layer


for i in range(1, count):
    bfs(i)

print("The sum of the shortest paths between the galaxies is: ", sum(dist.values()))
