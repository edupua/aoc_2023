from pathlib import Path

path = Path(__file__).parent / "../input/5"

# open the input file
with open(path, "r") as file:
    content = file.read()
# split content based on different sections
sections = content.split("\n\n")


# store the seed numbers as pairs
seeds_str = sections[0].split(":")[-1]
numbers = seeds_str.split()

seed_pairs = [
    (int(numbers[2 * i]), int(numbers[2 * i]) + int(numbers[2 * i + 1]))
    for i in range(0, int(len(numbers) / 2))
]
seed_pairs.sort()


##
## The maps are the same
##

# store the seed-to-soil in a list with number:
buffer = sections[1].split(":\n")[-1]
ss = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    ss.append((int(source), int(dest), int(number)))

# store the seed-to-fertilizer map
buffer = sections[2].split(":\n")[-1]
sf = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    sf.append((int(source), int(dest), int(number)))

# fertilizer to water map
buffer = sections[3].split(":\n")[-1]
fw = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    fw.append((int(source), int(dest), int(number)))

# water to light map
buffer = sections[4].split(":\n")[-1]
wl = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    wl.append((int(source), int(dest), int(number)))

# light to temperature map
buffer = sections[5].split(":\n")[-1]
lt = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    lt.append((int(source), int(dest), int(number)))

# temperature to humidity
buffer = sections[6].split(":\n")[-1]
th = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    th.append((int(source), int(dest), int(number)))


# humidity to location
buffer = sections[7].split(":\n")[-1]
hl = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    hl.append((int(source), int(dest), int(number)))

# sort all maps
ss.sort()
sf.sort()
fw.sort()
wl.sort()
lt.sort()
th.sort()
hl.sort()


# now make relations but with ranges

# seed to soil
curr_map = ss
pairs = seed_pairs
curr_pairs = []
for start, end in pairs:
    index = 0
    source, dest, number = curr_map[0]
    flag = 0
    while index < len(curr_map) and end >= source:
        if start < source + number - 1:
            if start <= source:
                curr_pairs.append((start, source))
                start = source

            if end <= source + number - 1:
                curr_pairs.append((dest + (start - source), dest + (end - source)))
                flag = 1
                break

            curr_pairs.append((dest + (start - source), dest + (number - 1)))
            start = source + number

        index += 1
        if index == len(curr_map):
            break
        source, dest, number = curr_map[index]

    if flag == 0:
        curr_pairs.append((start, end))
soil_pairs = curr_pairs
soil_pairs.sort()


# soil to fertilizer

curr_map = sf
pairs = soil_pairs
curr_pairs = []
for start, end in pairs:
    index = 0
    source, dest, number = curr_map[0]
    flag = 0
    while index < len(curr_map) and end >= source:
        if start < source + number - 1:
            if start <= source:
                curr_pairs.append((start, source))
                start = source

            if end <= source + number - 1:
                curr_pairs.append((dest + (start - source), dest + (end - source)))
                flag = 1
                break

            curr_pairs.append((dest + (start - source), dest + (number - 1)))
            start = source + number

        index += 1
        if index == len(curr_map):
            break
        source, dest, number = curr_map[index]

    if flag == 0:
        curr_pairs.append((start, end))
fert_pairs = curr_pairs
fert_pairs.sort()


# fertilizer to water
curr_map = fw
pairs = fert_pairs
curr_pairs = []
for start, end in pairs:
    index = 0
    source, dest, number = curr_map[0]
    flag = 0
    while index < len(curr_map) and end >= source:
        if start < source + number - 1:
            if start <= source:
                curr_pairs.append((start, source))
                start = source

            if end <= source + number - 1:
                curr_pairs.append((dest + (start - source), dest + (end - source)))
                flag = 1
                break

            curr_pairs.append((dest + (start - source), dest + (number - 1)))
            start = source + number

        index += 1
        if index == len(curr_map):
            break
        source, dest, number = curr_map[index]

    if flag == 0:
        curr_pairs.append((start, end))
water_pairs = curr_pairs
water_pairs.sort()


# water to light
curr_map = wl
pairs = water_pairs
curr_pairs = []
for start, end in pairs:
    index = 0
    source, dest, number = curr_map[0]
    flag = 0
    while index < len(curr_map) and end >= source:
        if start < source + number - 1:
            if start <= source:
                curr_pairs.append((start, source))
                start = source

            if end <= source + number - 1:
                curr_pairs.append((dest + (start - source), dest + (end - source)))
                flag = 1
                break

            curr_pairs.append((dest + (start - source), dest + (number - 1)))
            start = source + number

        index += 1
        if index == len(curr_map):
            break
        source, dest, number = curr_map[index]

    if flag == 0:
        curr_pairs.append((start, end))
light_pairs = curr_pairs
light_pairs.sort()


# light to temperature
curr_map = lt
pairs = light_pairs
curr_pairs = []
for start, end in pairs:
    index = 0
    source, dest, number = curr_map[0]
    flag = 0
    while index < len(curr_map) and end >= source:
        if start < source + number - 1:
            if start <= source:
                curr_pairs.append((start, source))
                start = source

            if end <= source + number - 1:
                curr_pairs.append((dest + (start - source), dest + (end - source)))
                flag = 1
                break

            curr_pairs.append((dest + (start - source), dest + (number - 1)))
            start = source + number

        index += 1
        if index == len(curr_map):
            break
        source, dest, number = curr_map[index]

    if flag == 0:
        curr_pairs.append((start, end))
temp_pairs = curr_pairs
temp_pairs.sort()


# temperature to humidity
curr_map = th
pairs = temp_pairs
curr_pairs = []
for start, end in pairs:
    index = 0
    source, dest, number = curr_map[0]
    flag = 0
    while index < len(curr_map) and end >= source:
        if start < source + number - 1:
            if start <= source:
                curr_pairs.append((start, source))
                start = source

            if end <= source + number - 1:
                curr_pairs.append((dest + (start - source), dest + (end - source)))
                flag = 1
                break

            curr_pairs.append((dest + (start - source), dest + (number - 1)))
            start = source + number

        index += 1
        if index == len(curr_map):
            break
        source, dest, number = curr_map[index]

    if flag == 0:
        curr_pairs.append((start, end))
hum_pairs = curr_pairs
hum_pairs.sort()


# humidity to location
curr_map = hl
pairs = hum_pairs
curr_pairs = []
for start, end in pairs:
    index = 0
    source, dest, number = curr_map[0]

    # flag 1 means no need of including overlap
    flag = 0
    while index < len(curr_map) and end >= source:
        if start < source + number - 1:
            if start <= source:
                curr_pairs.append((start, source))
                start = source

            if end <= source + number - 1:
                curr_pairs.append((dest + (start - source), dest + (end - source)))
                flag = 1
                break

            curr_pairs.append((dest + (start - source), dest + (number - 1)))
            start = source + number

        index += 1
        if index == len(curr_map):
            break
        source, dest, number = curr_map[index]

    if flag == 0:
        curr_pairs.append((start, end))
location_pairs = curr_pairs
location_pairs.sort()

## it is necessary to handle edge cases and remove single seed answers without ranges
location_pairs = [pair for pair in location_pairs if pair[0] != pair[1]]


print("The lowest location numbers is: ", min([pair[0] for pair in location_pairs]))
