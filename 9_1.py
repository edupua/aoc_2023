from pathlib import Path

path = Path(__file__).parent / "../input/9"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()


""" find diff function finds the number to add to get next element """


def find_diff(series):
    if series.count(0) == len(series):
        return 0
    diff_series = [series[i + 1] - series[i] for i in range(len(series) - 1)]

    return diff_series[-1] + find_diff(diff_series)


# parse each line and pass it into series
history_sum = 0
for line in lines:
    series = [int(str) for str in line.split()]

    history_sum += series[-1] + find_diff(series)

print("The sum of extrapolated values is:", history_sum)
