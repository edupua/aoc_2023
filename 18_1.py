from pathlib import Path
from pprint import pprint
from sys import setrecursionlimit

setrecursionlimit(540000)

path = Path(__file__).parent / "../input/18"
n = 750  # 671 is max
m = 720  # 704 is max

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()
grid = [["."] * m for _ in range(n)]

dir_map = {"R": (0, 1), "D": (1, 0), "L": (0, -1), "U": (-1, 0)}
shift = {(-1, 0), (0, -1), (0, 1), (1, 0)}

# finds appropriate grid
i, j = 50, 10
grid[50][10] = "#"
for line in lines:
    d, num, _ = line.split()
    di, dj = dir_map[d]
    if not (0 <= i < n and 0 <= j < m):
        print(i, j)
    for _ in range(int(num)):
        i += di
        j += dj
        grid[i][j] = "#"


# dfs which populates the outside ones
vis = set()
stack = [(0, 0)]
while stack:
    i, j = stack.pop()
    if (i, j) in vis or not (0 <= i < n and 0 <= j < m) or grid[i][j] == "#":
        continue
    vis.add((i, j))
    for di, dj in shift:
        stack.append((i + di, j + dj))


new_grid = [["#"] * m for _ in range(n)]
for i, j in vis:
    new_grid[i][j] = "."

for i, line in enumerate(new_grid):
    print(i - 50, line.count("#"))
# for line in grid:
#    print(line)

# subtract the outside to get the inside ones
print("The volume of the lagoon is:", m * n - len(vis))
