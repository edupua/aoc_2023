from pathlib import Path
from math import prod

path = Path(__file__).parent / "../input/19"

# open the input file
with open(path, "r") as file:
    workflows, _ = file.read().split("\n\n")

info_map = {
    "x": 0,
    "m": 1,
    "a": 2,
    "s": 3,
}


def intersection(a: tuple[int, int], b: tuple[int, int]):
    inter = (max(a[0], b[0]), min(a[1], b[1]))
    residue = (0, 0)

    # empty case
    if inter[0] >= inter[1]:
        return [(0, 0), (0, 0)]

    if a[0] < inter[0] and a[1] == inter[1]:
        residue = (a[0], inter[0])
    elif a[1] > inter[1] and a[0] == inter[0]:
        residue = (inter[1], a[1])
    elif a[1] == inter[1] and a[0] == inter[0]:
        residue = (0, 0)

    return inter, residue


w_dict = {}
for line in workflows.splitlines():
    name, conditions = line.split("{")
    conditions = conditions[:-1]
    condition_list = conditions.split(",")
    c_list = []
    for condition in condition_list:
        if ":" in condition:
            ineq_str, dest = condition.split(":")
            if "<" in ineq_str:
                para, value = ineq_str.split("<")
                pair = (1, int(value))
            elif ">" in ineq_str:
                para, value = ineq_str.split(">")
                pair = (int(value) + 1, 4001)
            ineq = (info_map[para], pair)
        else:
            ineq = (0, (1, 4001))
            dest = condition
        c_list.append((ineq, dest))
    w_dict.update({name: c_list})

allowed = []
total = 0


def rec(ranges, wf):
    global total
    if wf == "A":
        allowed.append(ranges)
        total += prod(b - a for a, b in ranges)
        return
    elif wf == "R":
        return

    for (para, intended_range), dest in w_dict[wf]:
        source_range = ranges[para]
        inter, residue = intersection(source_range, intended_range)
        if inter != (0, 0):
            ranges[para] = inter
            rec(ranges.copy(), dest)
        if residue == (0, 0):
            break
        ranges[para] = residue
    return


initial_ranges = [(1, 4001), (1, 4001), (1, 4001), (1, 4001)]
rec(initial_ranges, "in")

print("The total number of possibility of acceptance is:", total)
