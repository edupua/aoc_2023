from pathlib import Path
from pprint import pprint
import sys

sys.setrecursionlimit(19881)

path = Path(__file__).parent / "../input/23"


# graph reduction method but takes like 50 seconds


# open the input file
with open(path, "r") as file:
    lines = file.read().split()
grid = [list(line) for line in lines]
n = len(grid)
m = len(grid[0])

shift = set([(-1, 0), (0, -1), (0, 1), (1, 0)])
start = (0, grid[0].count("."))


def get_neighbours(node):
    i, j = node
    bad_set = set()
    for di, dj in shift:
        if not (0 <= i + di < n and 0 <= j + dj < m) or grid[i + di][j + dj] == "#":
            bad_set.add((di, dj))
    return set([(i + di, j + dj) for di, dj in shift - bad_set])


graph = {0: set()}
nei_graph = {0: set()}
j_set = {}

name_count = 0
vis = set()
end_name = 0


def shrink(name, root, node):
    global end_name
    global name_count
    vis.add(node)
    neis = get_neighbours(node) - set([root])
    if len(neis) == 0:
        graph[name].add(node)
        end_name = name
    elif len(neis) == 1:
        graph[name].add(node)
        nei = list(neis)[0]
        if nei not in vis:
            shrink(name, node, nei)
        else:
            j_name = j_set[nei]
            nei_graph[j_name].add(name)
            nei_graph[name].add(j_name)

    elif len(neis) >= 2:
        j_name = name_count + 1
        name_count += 1

        j_set.update({node: j_name})

        graph.update({j_name: set([node])})
        nei_graph.update({j_name: set([name])})
        nei_graph[name].add(j_name)

        for nei in neis:
            if nei not in vis:
                new_name = name_count + 1
                name_count += 1
                graph.update({new_name: set()})
                nei_graph.update({new_name: set([j_name])})
                nei_graph[j_name].add(new_name)

                shrink(new_name, node, nei)


def calculate(path):
    path_sum = 0
    for node in path:
        path_sum += len(graph[node])
    return path_sum


max_path = 0


def dfs(node, path):
    global max_path
    path.add(node)
    if node == end_name:
        max_path = max(max_path, calculate(path))
    else:
        for nei in nei_graph[node]:
            if nei not in path:
                dfs(nei, path)
    path.remove(node)


shrink(0, (0, 0), start)
dfs(0, set())

print("The length of longest path is:", max_path - 1)
