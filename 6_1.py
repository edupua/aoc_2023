import math
from pathlib import Path

path = Path(__file__).parent / "../input/6"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

times = lines[0].split(":")[-1].split()
dists = lines[1].split(":")[-1].split()

# quadratic equation solving you get two roots: x^2 - Tx + d

answer_error = 1
for i in range(len(times)):
    T = int(times[i])
    d = int(dists[i])

    # take roof of lower root and floor of bigger root, but edge cases are different
    try:
        lower = int((T - (T**2 - 4 * d) ** 0.5) / 2) + 1
    except TypeError:
        # 0 roots case
        answer_error = 0
        continue

    upper = math.ceil(((T + (T**2 - 4 * d) ** 0.5) / 2) - 1)

    # answer is number of numbers between lower and upper including them
    answer = upper - lower + 1
    answer_error *= answer

print("The error in the margin of winning is: ", answer_error)
