from pathlib import Path
from collections import Counter
from functools import cmp_to_key

path = Path(__file__).parent / "../input/7_sample"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()


# make a seperate function for comparing hands
def compare_hands(a, b):
    counter_a, counter_b = Counter(a), Counter(b)

    # count how  many similar items
    if len(counter_a) > len(counter_b):
        return -1
    elif len(counter_a) < len(counter_b):
        return 1

    if counter_a.most_common(1)[0][1] > counter_b.most_common(1)[0][1]:
        return 1
    elif counter_a.most_common(1)[0][1] < counter_b.most_common(1)[0][1]:
        return -1

    return 0


# make a function for comparing each card of the hand
def compare_chars(a, b):
    if a.isdigit() and b.isdigit():
        return int(a) - int(b)
    if a.isdigit():
        return -1
    if b.isdigit():
        return 1

    ## order list
    order_list = ["T", "J", "Q", "K", "A"]
    a_pos = -1
    b_pos = -1

    for i, letter in enumerate(order_list):
        if letter == a:
            a_pos = i
        if letter == b:
            b_pos = i

    return a_pos - b_pos


# use a custom comparator, assuming hands are unique
def compare(a, b):
    hand_a, hand_b = a[0], b[0]

    h_comp = compare_hands(hand_a, hand_b)

    if h_comp != 0:
        return h_comp

    for i in range(len(hand_a)):
        c_comp = compare_chars(hand_a[i], hand_b[i])
        if c_comp != 0:
            return c_comp

    return 0


hands = []
# make a list of tuples
for line in lines:
    hand, bid = line.split()
    hands.append((hand, int(bid)))

hands.sort(key=cmp_to_key(compare))

# now find the sum of hands

hand_sum = 0
for i in range(len(hands)):
    _, hand = hands[i]
    hand_sum += (i + 1) * hand

print("The sum of total winnings is: ", hand_sum)
