from pathlib import Path
from pprint import pprint

path = Path(__file__).parent / "../input/19"

# open the input file
with open(path, "r") as file:
    workflows, parts = file.read().split("\n\n")

info_map = {
    "x": 0,
    "m": 1,
    "a": 2,
    "s": 3,
}

w_dict = {}
for line in workflows.splitlines():
    name, conditions = line.split("{")
    conditions = conditions[:-1]
    condition_list = conditions.split(",")
    c_list = []
    for condition in condition_list:
        if ":" in condition:
            ineq_str, dest = condition.split(":")
            flag = 0
            if "<" in ineq_str:
                para, value = ineq_str.split("<")
                flag = 1
            elif ">" in ineq_str:
                para, value = ineq_str.split(">")
            else:
                print("skill issues")
            ineq = (info_map[para], int(value), flag)
        else:
            ineq = (0, 0, 0)
            dest = condition
        c_list.append((ineq, dest))
    w_dict.update({name: c_list})


def rec(part_info: tuple[int, int, int, int], wf: str) -> bool:
    # print(part_info, wf)
    if wf == "A":
        return True
    elif wf == "R":
        return False

    for (para, value, cmp), dest in w_dict[wf]:
        if cmp == 0 and part_info[para] > value:
            return rec(part_info, dest)
        if cmp == 1 and part_info[para] < value:
            return rec(part_info, dest)


# pprint(w_dict)
total = 0
for part in parts.splitlines():
    part = part[1:-1]
    part_info = tuple(int(info[2:]) for info in part.split(","))

    if rec(part_info, "in"):
        # print(part_info, "accepted")
        total += sum(part_info)

print("The total rating numbers for all the accepted parts: ", total)
