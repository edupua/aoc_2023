from pathlib import Path

path = Path(__file__).parent / "../input/3"

# open the input file
with open(path, "r") as file:
    file_contents = file.read()

# make a grid out of input
grid = [list(line) for line in file_contents.splitlines()]

n = len(grid)
m = len(grid[0])

sum = 0
for i in range(n):
    for j in range(m):
        if grid[i][j] == "*":
            num_list = []

            # above cases
            if i > 0 and j > 0 and grid[i - 1][j - 1].isdigit():
                k = 0
                number = ""
                while j - 1 - k >= 0 and grid[i - 1][j - 1 - k].isdigit():
                    number = grid[i - 1][j - 1 - k] + number
                    k += 1
                num_list.append(number)

            if i > 0 and j < m - 1 and grid[i - 1][j + 1].isdigit():
                k = 0
                number = ""
                while j + 1 + k <= m - 1 and grid[i - 1][j + 1 + k].isdigit():
                    number += grid[i - 1][j + 1 + k]
                    k += 1
                num_list.append(number)

            # manually check for vertically above
            if i > 0 and grid[i - 1][j].isdigit():
                if j < m - 1 and grid[i - 1][j + 1].isdigit():
                    num_list[-1] = grid[i - 1][j] + num_list[-1]
                    if grid[i - 1][j - 1].isdigit():
                        num_list[-2] += num_list[-1]
                        num_list.pop()

                elif j > 0 and grid[i - 1][j - 1].isdigit():
                    num_list[-1] += grid[i - 1][j]

                else:
                    num_list.append(grid[i - 1][j])

            # below cases
            if i < n - 1 and j > 0 and grid[i + 1][j - 1].isdigit():
                k = 0
                number = ""
                while j - 1 - k >= 0 and grid[i + 1][j - 1 - k].isdigit():
                    number = grid[i + 1][j - 1 - k] + number
                    k += 1
                num_list.append(number)

            if i < n - 1 and j < m - 1 and grid[i + 1][j + 1].isdigit():
                k = 0
                number = ""
                while j + 1 + k <= m - 1 and grid[i + 1][j + 1 + k].isdigit():
                    number += grid[i + 1][j + 1 + k]
                    k += 1
                num_list.append(number)

            # manually check for vertically below
            if i < n - 1 and grid[i + 1][j].isdigit():
                if j < m - 1 and grid[i + 1][j + 1].isdigit():
                    num_list[-1] = grid[i + 1][j] + num_list[-1]
                    if grid[i + 1][j - 1].isdigit():
                        num_list[-2] += num_list[-1]
                        num_list.pop()

                elif j > 0 and grid[i + 1][j - 1].isdigit():
                    num_list[-1] += grid[i + 1][j]
                else:
                    num_list.append(grid[i + 1][j])

            # left case
            if j > 0 and grid[i][j - 1].isdigit():
                k = 0
                number = ""
                while j - 1 - k >= 0 and grid[i][j - 1 - k].isdigit():
                    number = grid[i][j - 1 - k] + number
                    k += 1
                num_list.append(number)

            # right case
            if j < m - 1 and grid[i][j + 1].isdigit():
                k = 0
                number = ""
                while j + 1 + k <= m - 1 and grid[i][j + 1 + k].isdigit():
                    number += grid[i][j + 1 + k]
                    k += 1
                num_list.append(number)

            temp_list = sorted([int(num) for num in num_list])
            print(temp_list)

            if len(num_list) == 2:
                sum += int(num_list[0]) * int(num_list[1])

print("The sum of gears is: ", sum)
