from email.policy import default
from pathlib import Path
from pprint import pprint
from functools import cache
from collections import defaultdict

path = Path(__file__).parent / "../input/14"

# open the input file
with open(path, "r") as file:
    grid = [list(line) for line in file.read().splitlines()]
n = len(grid)
m = len(grid[0])


def rotate_clockwise(grid):
    grid = [list(tup) for tup in list(zip(*grid[::-1]))]
    return grid


# tilting


def tilt(grid):
    for j in range(m):
        weights = []
        column = [grid[r][j] for r in range(n)]
        empty = column.index(".") if "." in column else -1
        for i in range(n):
            if grid[i][j] == "O" and empty < i:
                grid[empty][j], grid[i][j], column[empty], column[i] = (
                    "O",
                    ".",
                    "O",
                    ".",
                )
                empty = column.index(".") if "." in column else 101
            elif grid[i][j] == "#":
                for k in range(i):
                    column[k] = "#"
                empty = column.index(".") if "." in column else 101
    return grid


def calculate(grid):
    n = len(grid)
    m = len(grid[0])
    weights_sum = sum(
        [(n - i) for i in range(m) for j in range(n) if grid[i][j] == "O"]
    )
    return weights_sum


prev_grid = []
value_dict = defaultdict(set)
for i in range(200):
    grid = tilt(grid)  # north
    grid = rotate_clockwise(grid)  # west is up
    grid = tilt(grid)  # west
    grid = rotate_clockwise(grid)  # south is up
    grid = tilt(grid)  # south
    grid = rotate_clockwise(grid)  # east is up
    grid = tilt(grid)  # east
    grid = rotate_clockwise(grid)  # back to north is up

    # pprint(grid)
    value = calculate(grid)
    value_dict[value].add(i + 1)
    prev_grid = grid

# now use the logic
longest = sorted(
    list(max([value for value in value_dict.values()], key=lambda x: len(x)))
)
divisor = longest[-1] - longest[-2]

# find what reminder gives you the required value
rem = 1000000000 % divisor

# find maximimum number below 200 which gives the same output
number = divisor * (int(200 / divisor) - 1) + rem

answer = [key for key, value in value_dict.items() if number in value][0]

# find the value at that number
print("The final load on north after 1,000,000,000 cycles is: ", answer)
