from pathlib import Path
from collections import defaultdict

path = Path(__file__).parent / "../input/4"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

# make a dictionary for points
matches = {}

# iterate through lines and split wining vs. given
n = len(lines)

for line in lines:
    card_no = line.split(":")[0].split()[-1]
    winning_str, given_str = line.split(":")[-1].split("|")

    winning = set(winning_str.split())
    given = set(given_str.split())

    count = len(winning.intersection(given))
    matches.update({card_no: count})


# make a new dictionary for number of cards, 1 by default
cards_count = defaultdict(lambda: 1)

# make a sorted list of keys
sorted_keys = sorted([int(key) for key in matches.keys()])

# now iterate through each match and update the dictionary
for key in sorted_keys:
    value = matches[str(key)]

    for count in range(1, value + 1):
        if str(key + count) not in matches.keys():
            break

        # increment by how many times current is being read
        cards_count[str(key + count)] += cards_count[str(key)]

# find the sum of the default dict using keys from matches dict
total_cards = sum(cards_count[key] for key in matches.keys())

matches = print("The total number of scratchcards is: ", total_cards)
