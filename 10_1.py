from pathlib import Path
import sys

# could have used stacks but whatever this is the hack
sys.setrecursionlimit(19600)

path = Path(__file__).parent / "../input/10"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

# make a grid
grid = [list(line) for line in lines]
n = len(grid)
m = len(grid[0])

source = [
    (i, j) for j in range(len(grid[0])) for i in range(len(grid)) if grid[i][j] == "S"
][0]

shift = [(-1, 0), (1, 0), (0, -1), (0, 1)]

""" returns the neighbours that are allowed for given i,j """


def allowed_neighbours(i, j):
    if grid[i][j] == "S":
        return shift
    if grid[i][j] == "|":
        return [(-1, 0), (1, 0)]
    if grid[i][j] == "-":
        return [(0, -1), (0, 1)]
    if grid[i][j] == "L":
        return [(-1, 0), (0, 1)]
    if grid[i][j] == "J":
        return [(-1, 0), (0, -1)]
    if grid[i][j] == "7":
        return [(1, 0), (0, -1)]
    if grid[i][j] == "F":
        return [(1, 0), (0, 1)]
    else:
        return []


# use dfs in such a way that visited array is only the current loop
visited = [[-1] * m for i in range(n)]
s_i, s_j = source[0], source[1]
loops = []


def dfs(i, j, loop):
    loop.add((i, j))
    allowed = [
        (i + di, j + dj)
        for di, dj in allowed_neighbours(i, j)
        if 0 <= i + di < n and 0 <= j + dj < m
    ]

    for I, J in allowed:
        # the neighbour should also be aligned to accept
        rev_allowed = [
            (I + di, J + dj)
            for di, dj in allowed_neighbours(I, J)
            if 0 <= I + di < n and 0 <= J + dj < m
        ]
        if not (i, j) in rev_allowed:
            continue

        if (I, J) not in loop:
            dfs(I, J, loop)
        elif I == s_i and J == s_j:
            loops.append(len(loop))


dfs(s_i, s_j, set())

print(
    "Thus the loop that takes maximum steps has last element at: ", int(max(loops) / 2)
)
