from pathlib import Path
from pprint import pprint
import numpy as np
import math

path = Path(__file__).parent / "../input/21"

# I got filtered here. So had to look up on this shit, its some 'quadratic recurrence relation' not even linear convolution.
# The method that makes send to me is just doing some "curve fitting".
# Found the answer at points 65, 196, 327 because n = 131 you know,
# get 3 values to find coefficients of a quadratic polynomial and there u go.

# Today's adventofmath problem was very challenging

# open the input file
with open(path, "r") as file:
    lines = file.read().split()
grid = [list(line) for line in lines]
n, m = len(grid), len(grid[0])

shift = [(-1, 0), (0, -1), (0, 1), (1, 0)]

source = tuple()
for i in range(n):
    for j in range(m):
        if grid[i][j] == "S":
            source = (i, j)

s_i, s_j = source
grid[s_i][s_j] = "."


data = []
layer = [source]
for t in range(350):
    new_layer = []
    vis = set()
    for i, j in layer:
        neis = [
            (i + di, j + dj)
            for di, dj in shift
            if grid[(i + di) % n][(j + dj) % m] != "#"
        ]
        for nei in neis:
            if nei not in vis:
                vis.add(nei)
                new_layer.append(nei)
    layer = new_layer

    if t == 64 or t == 195 or t == 326:
        data.append((t // n, len(layer)))

x_data, y_data = zip(*data)
degree = 2
coefficients = np.polyfit(x_data, y_data, degree)
my_poly = np.poly1d(coefficients)
x_value = 26501365 // n
y_value_at_x = my_poly(x_value)

print(
    "The number of gardens covered in taking 26501365 steps is:",
    math.ceil(y_value_at_x),
)
