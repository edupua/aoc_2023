from pathlib import Path
from pprint import pprint

path = Path(__file__).parent / "../input/11"

# open the input file
with open(path, "r") as file:
    rows = file.read().splitlines()

grid = list(list(row) for row in rows)
n = len(grid)
m = len(grid[0])

# insert -1 in rows: denoting wall
for i in range(n):
    if grid[i].count("#") == 0:
        grid[i] = [-1] * m

# insert -1 in columns
for j in range(m):
    column = [grid[i][j] for i in range(n)]
    if column.count("#") == 0:
        for i in range(n):
            grid[i][j] = -1


# assign numbers and store locations
locations = {}
count = 1
for i in range(n):
    for j in range(m):
        if grid[i][j] == "#":
            grid[i][j] = count
            locations.update({count: (i, j)})
            count += 1
        if grid[i][j] == ".":
            grid[i][j] = 0

# make a dictionary for shortest distances:
dist = {}

shift = [(-1, 0), (0, -1), (0, 1), (1, 0)]


""" this also store number of times -1 got crossed """


def mod_bfs(source):
    s_i, s_j = locations[source]
    l = 0
    vis = set()
    layer = [(s_i, s_j, 0)]

    while layer:
        new_layer = []
        for i, j, d in layer:
            if grid[i][j] != 0:
                if grid[i][j] == -1:
                    d += 1
                elif grid[i][j] > source:
                    dist.update({(source, grid[i][j]): (l, d)})

            nei = [
                (i + di, j + dj)
                for di, dj in shift
                if 0 <= i + di < n and 0 <= j + dj < m
            ]

            for I, J in nei:
                if (I, J) not in vis:
                    vis.add((I, J))
                    new_layer.append((I, J, d))

        l += 1
        layer = new_layer


for i in range(1, count):
    mod_bfs(i)

# empty row number
e = 999999
print(
    "The sum of the shortest paths between the galaxies with increased distance is: ",
    sum([l + d * e for l, d in dist.values()]),
)
