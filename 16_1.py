from pathlib import Path
from pprint import pprint
#from functools import cache
import sys

# could have used stacks but whatever this is the hack
sys.setrecursionlimit(540000)

path = Path(__file__).parent / "../input/16"

# open the input file
with open(path, "r") as file:
    lines = file.read().replace('\\','b').splitlines()
    grid = [ list(line) for line in lines ]

n = len(grid)
m = len(grid[0])

#pprint(grid)

hashmap = {}

def rec(i,j, direction) -> set(tuple[int,int]):

    if not ( 0 <= i < n and 0 <= j < m):
        return set()

    if (i,j,direction) in hashmap.keys():
        return hashmap[(i,j,direction)]

    ret_set = set()
    hashmap.update({(i,j,direction): set()})


    match direction:
        case 1:
            match grid[i][j]:
                case '.' | '-':
                    ret_set = ret_set.union(rec(i, j+1, 1))
                case '|':
                    ret_set = ret_set.union(rec(i-1, j, 4))
                    ret_set = ret_set.union( rec(i+1, j, 2))
                case 'b':
                    ret_set = ret_set.union( rec(i+1, j, 2))
                case '/':
                    ret_set = ret_set.union( rec(i-1, j, 4))
                case _:
                    print("skill isseu 1")
        case 2:
            match grid[i][j]:
                case '.' | '|':
                    ret_set = ret_set.union( rec(i+1,j,2))
                case '-':
                    ret_set = ret_set.union( rec(i,j-1,3))
                    ret_set = ret_set.union( rec(i,j+1,1))
                case 'b':
                    ret_set = ret_set.union( rec(i,j+1,1))
                case '/':
                    ret_set = ret_set.union( rec(i,j-1,3))
                case _:
                    print("skill isseu 2")
        case 3:
            match grid[i][j]:
                case '.' | '-':
                    ret_set = ret_set.union( rec(i,j-1,3))
                case '|':
                    ret_set = ret_set.union( rec(i-1,j,4))
                    ret_set = ret_set.union( rec(i+1,j,2))
                case 'b':
                    ret_set = ret_set.union( rec(i-1,j,4))
                case '/':
                    ret_set = ret_set.union( rec(i+1,j,2))
                case _:
                    print("skill isseu 3")
        case 4:
            match grid[i][j]:
                case '.' | '|':
                    ret_set = ret_set.union( rec(i-1,j,4))
                case '-':
                    ret_set = ret_set.union( rec(i,j-1,3))
                    ret_set = ret_set.union( rec(i,j+1,1))
                case 'b':
                    ret_set = ret_set.union( rec(i,j-1,3))
                case '/':
                    ret_set = ret_set.union( rec(i,j+1,1))
                case _:
                    print("skill isseu 4")
        case _:
            print("mega skill issue")
    
    ret_set.add((i,j))
    hashmap.update({(i,j,direction): ret_set})
    return ret_set

print("The number of tiles being energised are", len(rec(0,0,1)) )
