from sympy import symbols, Eq, solve, Point, Line

x, y, x1, y1, x2, y2, dx1, dy1, dx2, dy2 = symbols("x y x1 y1 x2 y2 dx1 dy1 dx2 dy2")

eq1 = Eq((y - y1) / dy1, (x - x1) / dx1)
eq2 = Eq((y - y2) / dy2, (x - x2) / dx2)

answer = solve([eq1, eq2], (x, y))
print(answer)
