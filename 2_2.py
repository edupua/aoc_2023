from pathlib import Path

path = Path(__file__).parent / "../input/2"
# open the input file
with open(path, "r") as file:
    lines = file.read().lines()

power_sum = 0

# iterate through each line and find game_id number
for line in lines:
    game_info, cube_info = line.split(":")

    # get the game id
    _, game_id = game_info.split()

    # get each color and check
    cubes_color_info = cube_info.replace(";", ",").split(",")

    max_red, max_blue, max_green = 0, 0, 0

    # iterate and find max
    for cubes in cubes_color_info:
        number, color = cubes.split()

        # convert number from string type to int
        number = int(number)

        if color == "red":
            max_red = max(max_red, number)

        if color == "green":
            max_green = max(max_green, number)

        if color == "blue":
            max_blue = max(max_blue, number)

    power = max_red * max_green * max_blue
    power_sum += power

print("The power sum is: ", power_sum)
