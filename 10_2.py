from pathlib import Path
import sys

# could have used stacks but whatever this is the hack
sys.setrecursionlimit(19600)

path = Path(__file__).parent / "../input/10"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

# make a grid
grid = [list(line) for line in lines]
n = len(grid)
m = len(grid[0])

source = [(i, j) for j in range(m) for i in range(n) if grid[i][j] == "S"][0]

shift = [(-1, 0), (1, 0), (0, -1), (0, 1)]

""" returns the neighbours that are allowed for given i,j """


def allowed_neighbours(i, j):
    if grid[i][j] == "S":
        return shift
    if grid[i][j] == "|":
        return [(-1, 0), (1, 0)]
    if grid[i][j] == "-":
        return [(0, -1), (0, 1)]
    if grid[i][j] == "L":
        return [(-1, 0), (0, 1)]
    if grid[i][j] == "J":
        return [(-1, 0), (0, -1)]
    if grid[i][j] == "7":
        return [(1, 0), (0, -1)]
    if grid[i][j] == "F":
        return [(1, 0), (0, 1)]
    else:
        return []


visited = [[-1] * m for i in range(n)]
s_i, s_j = source[0], source[1]
max_loop = []

""" use dfs in such a way that visited array is only the current loop """


def dfs(i, j, loop):
    global max_loop

    loop.append((i, j))
    allowed = [
        (i + di, j + dj)
        for di, dj in allowed_neighbours(i, j)
        if 0 <= i + di < n and 0 <= j + dj < m
    ]

    for I, J in allowed:
        # the neighbour should also accept
        rev_allowed = [
            (I + di, J + dj)
            for di, dj in allowed_neighbours(I, J)
            if 0 <= I + di < n and 0 <= J + dj < m
        ]
        if not (i, j) in rev_allowed:
            continue

        if (I, J) not in loop:
            dfs(I, J, loop)
        elif I == s_i and J == s_j:
            if len(loop) > len(max_loop):
                max_loop = loop


dfs(s_i, s_j, [])

new_visited = [[0] * m for i in range(n)]

# make all pipes in loop a wall: -1, unvisited: 0, visited: 1-2 (in and out)
for i, j in max_loop:
    new_visited[i][j] = -1

vis_mark = 1


# use normal dfs
def normal_dfs(i, j):
    new_visited[i][j] = vis_mark
    nei = [(i + di, j + dj) for di, dj in shift if 0 <= i + di < n and 0 <= j + dj < m]
    for I, J in nei:
        if new_visited[I][J] == 0:
            normal_dfs(I, J)


for i in range(n):
    for j in range(m):
        if new_visited[i][j] == 0:
            normal_dfs(i, j)
            vis_mark += 1

# make a set of all known scc
scc = set()
for i in range(n):
    for j in range(m):
        if new_visited[i][j] not in scc:
            scc.add(new_visited[i][j])

## now make a set of scc which touch border
outer = set()
# for i in range(n):
#    if new_visited[i][0] not in outer:
#        outer.add(new_visited[i][0])
#    if new_visited[i][m-1] not in outer:
#        outer.add(new_visited[i][m-1])
#
# for j in range(m):
#    if new_visited[0][j] not in outer:
#        outer.add(new_visited[0][j])
#    if new_visited[n-1][j] not in outer:
#        outer.add(new_visited[n-1][j])


L = len(max_loop)

# replace S
s_index = max_loop.index((s_i, s_j))
p_i, p_j = max_loop[(s_index - 1) % L]
n_i, n_j = max_loop[(s_index + 1) % L]

s_shift = sorted([(p_i - s_i, p_j - s_j), (n_i - s_i, n_j - s_j)])

# -1,0  0,-1  0,1  1,0
if s_shift == [(-1, 0), (0, -1)]:
    grid[s_i][s_j] = "J"
elif s_shift == [(-1, 0), (0, 1)]:
    grid[s_i][s_j] = "L"
elif s_shift == [(-1, 0), (1, 0)]:
    grid[s_i][s_j] = "|"
elif s_shift == [(0, -1), (0, 1)]:
    grid[s_i][s_j] = "-"
elif s_shift == [(0, -1), (1, 0)]:
    grid[s_i][s_j] = "7"
elif s_shift == [(0, 1), (1, 0)]:
    grid[s_i][s_j] = "F"

# new notation don't confuse with old s_i
starting_edge = min(max_loop)
s_i, s_j = starting_edge[0], starting_edge[1]
s_index = max_loop.index(starting_edge)


def map_direction(index_tuple, pd, prev):
    i, j = index_tuple
    curr = grid[i][j]

    if pd != sorted(pd):
        print("idiotidiot")

    if curr == "F":
        flag = 0
        if prev == "7":
            if pd == [(-1, 0), (0, 1)]:
                flag = 1
            elif pd == [(0, -1), (1, 0)]:
                flag = 2
        if prev == "J":
            if pd == [(-1, 0), (0, -1)]:
                flag = 1
            elif pd == [(0, 1), (1, 0)]:
                flag = 2
        if prev == "-":
            if pd == [(-1, 0)]:
                flag = 1
            elif pd == [(1, 0)]:
                flag = 2

        if prev == "L":
            if pd == [(0, -1), (1, 0)]:
                flag = 1
            elif pd == [(-1, 0), (0, 1)]:
                flag = 2
        if prev == "|":
            if pd == [(0, -1)]:
                flag = 1
            elif pd == [(0, 1)]:
                flag = 2
        if flag == 1:
            return [(-1, 0), (0, -1)]
        elif flag == 2:
            return [(0, 1), (1, 0)]
        else:
            print("skill issue F")

    if curr == "7":
        flag = 0
        if prev == "F":
            if pd == [(-1, 0), (0, -1)]:
                flag = 1
            elif pd == [(0, 1), (1, 0)]:
                flag = 2
        if prev == "J":
            if pd == [(0, 1), (1, 0)]:
                flag = 1
            elif pd == [(-1, 0), (0, -1)]:
                flag = 2
        if prev == "-":
            if pd == [(-1, 0)]:
                flag = 1
            elif pd == [(1, 0)]:
                flag = 2
        if prev == "|":
            if pd == [(0, 1)]:
                flag = 1
            elif pd == [(0, -1)]:
                flag = 2
        if prev == "L":
            if pd == [(-1, 0), (0, 1)]:
                flag = 1
            elif pd == [(0, -1), (1, 0)]:
                flag = 2
        if flag == 1:
            return [(-1, 0), (0, 1)]
        elif flag == 2:
            return [(0, -1), (1, 0)]

    if curr == "J":
        flag = 0
        if prev == "F":
            if pd == [(0, 1), (1, 0)]:
                flag = 1
            elif pd == [(-1, 0), (0, -1)]:
                flag = 2
        if prev == "7":
            if pd == [(-1, 0), (0, 1)]:
                flag = 1
            elif pd == [(0, -1), (1, 0)]:
                flag = 2
        if prev == "L":
            if pd == [(0, -1), (1, 0)]:
                flag = 1
            elif pd == [(-1, 0), (0, 1)]:
                flag = 2
        if prev == "-":
            if pd == [(1, 0)]:
                flag = 1
            elif pd == [(-1, 0)]:
                flag = 2
        if prev == "|":
            if pd == [(0, 1)]:
                flag = 1
            elif pd == [(0, -1)]:
                flag = 2

        if flag == 1:
            return [(0, 1), (1, 0)]
        elif flag == 2:
            return [(-1, 0), (0, -1)]

    if curr == "L":
        flag = 0
        if prev == "F":
            if pd == [(-1, 0), (0, -1)]:
                flag = 1
            elif pd == [(0, 1), (1, 0)]:
                flag = 2
        if prev == "7":
            if pd == [(0, -1), (1, 0)]:
                flag = 1
            elif pd == [(-1, 0), (0, 1)]:
                flag = 2
        if prev == "J":
            if pd == [(0, 1), (1, 0)]:
                flag = 1
            elif pd == [(-1, 0), (0, -1)]:
                flag = 2
        if prev == "|":
            if pd == [(0, -1)]:
                flag = 1
            elif pd == [(0, 1)]:
                flag = 2
        if prev == "-":
            if pd == [(1, 0)]:
                flag = 1
            elif pd == [(-1, 0)]:
                flag = 2

        if flag == 1:
            return [(0, -1), (1, 0)]
        elif flag == 2:
            return [(-1, 0), (0, 1)]

    if curr == "-":
        flag = 0
        if prev == "F":
            if pd == [(-1, 0), (0, -1)]:
                flag = 1
            elif pd == [(0, 1), (1, 0)]:
                flag = 2
        if prev == "7":
            if pd == [(-1, 0), (0, 1)]:
                flag = 1
            elif pd == [(0, -1), (1, 0)]:
                flag = 2
        if prev == "J":
            if pd == [(-1, 0), (0, -1)]:
                flag = 1
            elif pd == [(0, 1), (1, 0)]:
                flag = 2
        if prev == "L":
            if pd == [(-1, 0), (0, 1)]:
                flag = 1
            elif pd == [(0, -1), (1, 0)]:
                flag = 2
        if prev == "-":
            if pd == [(-1, 0)]:
                flag = 1
            elif pd == [(1, 0)]:
                flag = 2

        if flag == 1:
            return [(-1, 0)]
        elif flag == 2:
            return [(1, 0)]

    if curr == "|":
        flag = 0
        if prev == "F":
            if pd == [(-1, 0), (0, -1)]:
                flag = 1
            elif pd == [(0, 1), (1, 0)]:
                flag = 2
        if prev == "7":
            if pd == [(0, -1), (1, 0)]:
                flag = 1
            elif pd == [(-1, 0), (0, 1)]:
                flag = 2
        if prev == "J":
            if pd == [(-1, 0), (0, -1)]:
                flag = 1
            elif pd == [(0, 1), (1, 0)]:
                flag = 2
        if prev == "L":
            if pd == [(0, -1), (1, 0)]:
                flag = 1
            elif pd == [(-1, 0), (0, 1)]:
                flag = 2
        if prev == "|":
            if pd == [(0, -1)]:
                flag = 1
            elif pd == [(0, 1)]:
                flag = 2

        if flag == 1:
            return [(0, -1)]
        elif flag == 2:
            return [(0, 1)]


direction_out = []
if grid[s_i][s_j] == "F":
    direction_out = [(-1, 0), (0, -1)]
elif grid[s_i][s_j] == "7":
    direction_out = [(-1, 0), (0, 1)]
elif grid[s_i][s_j] == "-":
    direction_out = [(-1, 0)]

nei = [
    (s_i + di, s_j + dj)
    for di, dj in direction_out
    if 0 <= s_i + di < n and 0 <= s_j + dj < m and new_visited[s_i + di][s_j + dj] != -1
]
for I, J in nei:
    if new_visited[I][J] not in outer:
        outer.add(new_visited[I][J])

for index in range(1, L + 1):
    pi, pj = max_loop[(s_index + index - 1) % L]
    i, j = max_loop[(s_index + index) % L]
    direction_out = map_direction((i, j), direction_out, grid[pi][pj])
    nei = [
        (i + di, j + dj)
        for di, dj in direction_out
        if 0 <= i + di < n and 0 <= j + dj < m and new_visited[i + di][j + dj] != -1
    ]

    for I, J in nei:
        if new_visited[I][J] not in outer:
            outer.add(new_visited[I][J])

outer.add(-1)

inner_set = scc - outer

print(
    "The number of inner tiles is: ",
    sum([line.count(inner) for line in new_visited for inner in inner_set]),
)
