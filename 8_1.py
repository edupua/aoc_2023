from pathlib import Path
from itertools import cycle

path = Path(__file__).parent / "../input/8"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

directions = [0 if char == "L" else 1 for char in list(lines.pop(0))]
cycler = cycle(directions)

# only keep the nodes
lines.pop(0)

# make a adj_list for graph
adj = {}

for line in lines:
    node, nei_str = line.split(" = ")
    a_str, b_str = nei_str.split(", ")
    a = a_str[1:]
    b = b_str[:-1]
    adj.update({node: (a, b)})


# now perform graph traversal
curr = "AAA"
count = 0
while curr != "ZZZ":
    direction = next(cycler)
    count += 1

    curr = adj[curr][direction]

print("The number of steps required for every node to reach Z is: ", count)
