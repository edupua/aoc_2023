from pathlib import Path
from pprint import pprint

path = Path(__file__).parent / "../input/15"

# open the input file
with open(path, "r") as file:
    line = file.read()


def get_value(string):
    sumi = 0
    for char in string:
        sumi = ((ord(char) + sumi) * 17) % 256
    return sumi


boxes = {}
focus_map = {}

for step in line.split(","):
    if "=" in step:
        elements = step.replace("\n", "").split("=")
        if len(elements) > 2:
            print("parsed wrongly", elements)
        label, focus = elements[0], elements[1]
        box = get_value(label)

        if box in boxes.keys():
            if label not in boxes[box]:
                boxes[box].append(label)
        else:
            boxes.update({box: [label]})
        focus_map.update({label: int(focus)})

    elif "-" in step:
        label = step.split("-")[0]
        box = get_value(label)
        if box in boxes.keys() and label in boxes[box]:
            index = boxes[box].index(label)
            del boxes[box][index]

    else:
        print("skill issues", step)

print(
    "The sum of focussing power is: ",
    sum(
        (box + 1) * (i + 1) * (focus_map[slots[i]])
        for box, slots in boxes.items()
        for i in range(len(slots))
    ),
)
