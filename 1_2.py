from pathlib import Path

path = Path(__file__).parent / "../input/1"

# open the input file
with open(path, "r") as file:
    words = file.read().split()

possibilities = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}

sum = 0

for word in words:
    first, last = "0", "0"
    # find every word in every possible index
    for index in range(len(word)):
        for possibility in possibilities.keys():
            if word.startswith(possibility, index):
                first = possibilities[possibility]
            elif word[index].isdigit():
                first = word[index]
        if first != "0":
            break

    for index in range(len(word) - 1, -1, -1):
        for possibility in possibilities.keys():
            if word.startswith(possibility, index):
                last = possibilities[possibility]
            elif word[index].isdigit():
                last = word[index]
        if last != "0":
            break

    sum += int(first + last)


print("Your Callibration number is: ", sum)
