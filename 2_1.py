# limits
max_red = 12
max_green = 13
max_blue = 14

sum_id = 0


from pathlib import Path

path = Path(__file__).parent / "../input/2"

# open the input file
with open(path, "r") as file:
    words = file.read().split()

# iterate through each line and find game_id number
for line in lines:
    game_info, cube_info = line.split(":")

    # get the game id
    _, game_id = game_info.split()

    # get each color and check
    cubes_color_info = cube_info.replace(";", ",").split(",")
    for cubes in cubes_color_info:
        possible_flag = 1
        number, color = cubes.split()

        # convert number from string type to int
        number = int(number)

        if (
            (color == "red" and number > max_red)
            or (color == "green" and number > max_green)
            or (color == "blue" and number > max_blue)
        ):
            possible_flag = 0
            break

    sum_id += possible_flag * int(game_id)

print("The sum of all possible IDs is: ", sum_id)
