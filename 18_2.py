from pathlib import Path
from heapq import heapify, heappush, heappop
from pprint import pprint

path = Path(__file__).parent / "../input/18"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

# horizontal edges are of form { row number: [(col_start, col_end)] } in ascending order ( priority q)
hedges = {}
# vertical edges are of form [ ( col_num, (row_start, row_end) ) ]
vedges = []

i, j = 0, 0
for line in lines:
    _, _, string = line.split()
    string = string[2:-1]
    d = string[5:]
    num = int(string[:5], 16)
    # d, num, _ = line.split()
    # num = int(num)

    if d == "0":
        # if d == 'R':
        if i in hedges.keys():
            heappush(hedges[i], (j, j + num))
        else:
            hedges.update({i: [(j, j + num)]})
        j += num

    elif d == "1":
        # elif d == 'D':
        vedges.append((j, (i, i + num)))
        i += num

    elif d == "2":
        # elif d == 'L':
        if i in hedges.keys():
            heappush(hedges[i], (j - num, j))
        else:
            hedges.update({i: [(j - num, j)]})
        j -= num

    # elif d == 'U':
    elif d == "3":
        vedges.append((j, (i - num, i)))
        i -= num
    else:
        print("skill issues")

maxi, mini = max(hedges.keys()), min(hedges.keys())
vedges.sort()

total = 0
left_wall = 0
count = 0
for i in range(mini, maxi + 1):
    flag = 0  # flag sees if inside or out
    bflag = 0  # if on border or not if it is then is it top border(1) or bottom (2)

    if i not in hedges.keys() and i - 1 not in hedges.keys():
        total += count
        continue

    count = 0
    if i in hedges.keys():
        while hedges[i]:
            lj, rj = heappop(hedges[i])
            count += rj - lj + 1

    for j, (li, ri) in vedges:
        if li < i < ri:
            if flag == 0:
                flag = 1
                left_wall = j
            elif flag == 1:
                flag = 0
                count += j - left_wall + 1
                left_wall = 0

        elif li == i:
            if bflag == 0:
                bflag = 1
                if flag == 1:
                    count += j - left_wall
            elif bflag == 1:
                if flag == 1:
                    left_wall = j + 1
                bflag = 0

            elif bflag == 2:
                bflag = 0
                if flag == 0:
                    flag = 1
                    left_wall = j + 1
                elif flag == 1:
                    flag = 0
        elif ri == i:
            if bflag == 0:
                bflag = 2
                if flag == 1:
                    count += j - left_wall
            elif bflag == 2:
                if flag == 1:
                    left_wall = j + 1
                bflag = 0

            elif bflag == 1:
                bflag = 0
                if flag == 0:
                    flag = 1
                    left_wall = j + 1
                elif flag == 1:
                    flag = 0
    total += count

print("The volume of the lagoon is:", total)
