from pathlib import Path

path = Path(__file__).parent / "../input/15"

# open the input file
with open(path, "r") as file:
    line = file.read()


def get_value(string):
    sumi = 0
    for char in string:
        sumi = ((ord(char) + sumi) * 17) % 256
    return sumi


print(
    "The sum of the results is: ",
    sum(get_value(step.replace("\n", "")) for step in line.split(",")),
)
