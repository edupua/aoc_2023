from pathlib import Path

path = Path(__file__).parent / "../input/1"

# open the input file
with open(path, "r") as file:
    words = file.read().split()


# iterate through words to find callibration value
sum = 0
first, last = "0", "0"
for word in words:
    # first digit
    for letter in word:
        if letter.isdigit():
            first = letter
            break

    # find second digit
    for letter in reversed(word):
        if letter.isdigit():
            last = letter
            break

    number = int(first + last)
    sum += number

print("Your Callibration number is: ", sum)
