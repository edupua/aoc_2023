from pathlib import Path
from collections import defaultdict
from bisect import bisect_left

path = Path(__file__).parent / "../input/22"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

# format is name: [ ( (x_1,x_2), (y_1,y_2) ), (z_1,z_2) ]
locations = {}

temp_list = []
for line in lines:
    left, right = line.split("~")
    left_tup = tuple(int(num) for num in left.split(","))
    right_tup = tuple(int(num) for num in right.split(","))
    temp_list.append((left_tup, right_tup))

temp_list.sort(key=lambda x: (x[0][2], x[1][2]))
for i, value in enumerate(temp_list):
    (x_1, y_1, z_1), (x_2, y_2, z_2) = value
    locations.update({i + 1: [((x_1, x_2), (y_1, y_2)), (z_1, z_2)]})


def intersection(key, Key) -> bool:
    (x_1, x_2), (y_1, y_2) = locations[key][0]
    (X_1, X_2), (Y_1, Y_2) = locations[Key][0]
    if (
        x_1 <= x_2 < X_1 <= X_2
        or X_1 <= X_2 < x_1 <= x_2
        or y_1 <= y_2 < Y_1 <= Y_2
        or Y_1 <= Y_2 < y_1 <= y_2
    ):
        return False
    return True


sorted_list = sorted(
    [(value[1][1], (value[1][0], key)) for key, value in locations.items()]
)
for key in range(1, max(locations.keys()) + 1):
    (z_1, z_2) = locations[key][1]
    diff = z_2 - z_1
    index = bisect_left(sorted_list, (z_1, (1000, 0)))
    flag = 0
    for j in range(index - 1, -1, -1):
        Z_2, (Z_1, Key) = sorted_list[j]
        if Z_2 < z_1 and intersection(key, Key):
            z_1 = Z_2 + 1
            z_2 = z_1 + diff
            flag = 1
            break
    if flag == 0:
        z_1 = 1
        z_2 = z_1 + diff

    # update sorted list
    for i, (_, (_, k)) in enumerate(sorted_list):
        if key == k:
            sorted_list[i] = (z_2, (z_1, key))
            sorted_list.sort()
            break

    # update the dictionary
    locations[key][1] = (z_1, z_2)


supporting = defaultdict(list)
supportedby = defaultdict(list)

# another O(n^2) method to calculate the number of neighbours
sorted_locations = sorted(
    [(keys, items) for keys, items in locations.items()], key=lambda x: x[1][1]
)
for i, (key, value) in enumerate(sorted_locations):
    ((x_1, x_2), (y_1, y_2)), (z_1, z_2) = value
    for j in range(i - 1, -1, -1):
        Key, Value = sorted_locations[j]
        ((X_1, X_2), (Y_1, Y_2)), (Z_1, Z_2) = Value
        if Z_2 == z_1 - 1 and intersection(key, Key):
            supporting[Key].append(key)
            supportedby[key].append(Key)


# count number of removable bricks
count = 0
for key in locations.keys():
    items = supporting[key]
    if not items:
        # print(key, "is good")
        count += 1
        continue

    flag = 0
    for value in items:
        if len(supportedby[value]) == 1:
            flag = 1
            break
    if flag == 0:
        # print(key, "is good")
        count += 1

print(
    "The number of bricks that can be chosen as the one to get disintegrated are:",
    count,
)
