from pathlib import Path
from pprint import pprint

path = Path(__file__).parent / "../input/21"

# open the input file
with open(path, "r") as file:
    lines = file.read().split()
grid = [list(line) for line in lines]
n, m = len(grid), len(grid[0])

shift = [(-1, 0), (0, -1), (0, 1), (1, 0)]

source = tuple()
for i in range(n):
    for j in range(m):
        if grid[i][j] == "S":
            source = (i, j)

s_i, s_j = source
grid[s_i][s_j] = "."

layer = [source]
for _ in range(64):
    new_layer = []
    vis = set()
    for i, j in layer:
        neis = [
            (i + di, j + dj)
            for di, dj in shift
            if 0 <= i + di < n and 0 <= j + dj < m and grid[i + di][j + dj] != "#"
        ]
        for nei in neis:
            if nei not in vis:
                vis.add(nei)
                new_layer.append(nei)
    layer = new_layer

print("The number of garden plots reached by Elf is:", len(layer))
