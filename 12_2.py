from math import e
from pathlib import Path
from pickletools import read_stringnl_noescape_pair
from functools import cache

path = Path(__file__).parent / "../input/12"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()


@cache
def calculate_nums(sym):
    nums = []
    count = 0
    for char in sym:
        if char == "#":
            count += 1
        else:
            if count:
                nums.append(count)

            count = 0

    if count:
        nums.append(count)
    return tuple(nums)


@cache
def rec(sym, nums):
    # print(sym,nums)

    # if num is empty is either done
    if not nums:
        if sym and sym.count("#") != 0:
            # print("failed")
            return 0
        # print("pass")
        return 1
    elif "?" not in sym:
        if calculate_nums(sym) == nums:
            # print("pass")
            return 1
        # print("ass")
        return 0

    # if sym is empty then obs nowhere to go
    if not sym:
        # print("faili")
        return 0

    # if current # are greater than required then obv not valid
    if sym.count("#") > sum(nums):
        # print("fail")
        return 0

    # if current ? and # are not enough
    if sym.count("#") + sym.count("?") < sum(nums):
        # print("fails")
        return 0

    # find the first ?
    index = sym.index("?")
    # print("found ?", index)

    # try .

    extra = 0
    # check if nums upto curr ? is valid on adding dot (d)
    d_prev_nums = calculate_nums(sym[:index])
    if d_prev_nums == nums[: len(d_prev_nums)]:
        # upto this everything is valid thus pass the replaced list
        extra = rec(sym[index + 1 :], nums[len(d_prev_nums) :])
        # print(extra, "extra")
    elif sym[index - 1] != "#":
        # adding hashes won't fix it
        return 0

    # try #

    h_prev_nums = calculate_nums(sym[:index] + "#")
    if d_prev_nums == nums[: len(d_prev_nums)]:
        # adding hash made it not valid
        if h_prev_nums != nums[: len(h_prev_nums)]:
            # no more # can fix this
            if h_prev_nums[-1] > nums[: len(h_prev_nums)][-1]:
                return extra
            else:
                return extra + rec("#" + sym[index + 1 :], nums[len(d_prev_nums) :])

    # adding . didn't work so adding # may fix
    return extra + rec(sym[:index] + "#" + sym[index + 1 :], nums)


count_sum = 0
for line in lines:
    old_symbols = line.split(" ")[0]
    symbols = old_symbols
    for _ in range(4):
        symbols += "?" + old_symbols
    numbers = tuple([int(char) for char in line.split(" ")[-1].split(",")] * 5)

    possible_count = rec(symbols, numbers)
    count_sum += possible_count

    # returns the numbering of the current configuration


print("The sum of all the counts is", count_sum)
