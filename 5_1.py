from pathlib import Path

path = Path(__file__).parent / "../input/5"

# open the input file
with open(path, "r") as file:
    content = file.read()
# split content based on different sections
sections = content.split("\n\n")


# store the seed numbers
seeds_str = sections[0].split(":")[-1]
seeds = [int(seed) for seed in seeds_str.split()]


# store the seed-to-soil in a list with number:
buffer = sections[1].split(":\n")[-1]
ss = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    ss.append((int(source), int(dest), int(number)))

# store the seed-to-fertilizer map
buffer = sections[2].split(":\n")[-1]
sf = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    sf.append((int(source), int(dest), int(number)))

# fertilizer to water map
buffer = sections[3].split(":\n")[-1]
fw = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    fw.append((int(source), int(dest), int(number)))

# water to light map
buffer = sections[4].split(":\n")[-1]
wl = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    wl.append((int(source), int(dest), int(number)))

# light to temperature map
buffer = sections[5].split(":\n")[-1]
lt = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    lt.append((int(source), int(dest), int(number)))

# temperature to humidity
buffer = sections[6].split(":\n")[-1]
th = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    th.append((int(source), int(dest), int(number)))


# humidity to location
buffer = sections[7].split(":\n")[-1]
hl = []

for line in buffer.splitlines():
    dest, source, number = line.split()
    hl.append((int(source), int(dest), int(number)))

# now make relations

# seed to soil numbers for each seed
soils = []
for target in seeds:
    flag = 0
    for source, dest, number in ss:
        if source <= target <= source + number - 1:
            soils.append(dest + (target - source))
            flag = 1

    if flag == 0:
        soils.append(target)

# fertilizer numbers
ferts = []
for target in soils:
    flag = 0
    for source, dest, number in sf:
        if source <= target <= source + number - 1:
            ferts.append(dest + (target - source))
            flag = 1

    if flag == 0:
        ferts.append(target)

# water numbers
waters = []
for target in ferts:
    flag = 0
    for source, dest, number in fw:
        if source <= target <= source + number - 1:
            waters.append(dest + (target - source))
            flag = 1

    if flag == 0:
        waters.append(target)

# light numbers
lights = []
for target in waters:
    flag = 0
    for source, dest, number in wl:
        if source <= target <= source + number - 1:
            lights.append(dest + (target - source))
            flag = 1

    if flag == 0:
        lights.append(target)

# temperature numbers
temps = []
for target in lights:
    flag = 0
    for source, dest, number in lt:
        if source <= target <= source + number - 1:
            temps.append(dest + (target - source))
            flag = 1

    if flag == 0:
        temps.append(target)

# humidity numbers
hums = []
for target in temps:
    flag = 0
    for source, dest, number in th:
        if source <= target <= source + number - 1:
            hums.append(dest + (target - source))
            flag = 1

    if flag == 0:
        hums.append(target)

# location numbers
location = []
for target in hums:
    flag = 0
    for source, dest, number in hl:
        if source <= target <= source + number - 1:
            location.append(dest + (target - source))
            flag = 1

    if flag == 0:
        location.append(target)

print("The lowest location numbers is: ", min(location))
