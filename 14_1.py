from pathlib import Path
from pprint import pprint

path = Path(__file__).parent / "../input/14"

# open the input file
with open(path, "r") as file:
    grid = [list(line) for line in file.read().splitlines()]
n = len(grid)
m = len(grid[0])

# tilting
weights_sum = 0
for j in range(m):
    weights = []
    column = [grid[r][j] for r in range(n)]
    empty = column.index(".") if "." in column else -1
    for i in range(n):
        if grid[i][j] == "O":
            if empty < i:
                column[empty], column[i] = "O", "."
                weights_sum += n - empty
                empty = column.index(".") if "." in column else 101
            else:
                weights_sum += n - i
        elif grid[i][j] == "#":
            for k in range(i):
                column[k] = "#"
            empty = column.index(".") if "." in column else 101

print("The sum of loads on north are: ", weights_sum)
