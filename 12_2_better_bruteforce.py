from pathlib import Path
from pickletools import read_stringnl_noescape_pair

path = Path(__file__).parent / "../input/12"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()


# iterate through lines and use recursive backtracking

count_sum = 0
for I, line in enumerate([lines[0]]):
    # old_symbols = list(line.split(' ')[0])

    # make 5 times
    # symbols = old_symbols.copy()
    # for _ in range(4):
    #    symbols += ['?'] + old_symbols

    symbols = list(line.split(" ")[0])
    m = len(symbols) + 1
    numbers = [int(char) for char in line.split(" ")[-1].split(",")]
    n = len(numbers) + 1

    dp = [[0] * m for _ in range(n)]

    # print(numbers, symbols)


print("The sum of all the counts is", count_sum)
