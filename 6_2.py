import math
from pathlib import Path

path = Path(__file__).parent / "../input/6"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

T = int(lines[0].split(":")[-1].replace(" ", ""))
d = int(lines[1].split(":")[-1].replace(" ", ""))

# quadratic equation solving you get two roots: x^2 - Tx + d
# take roof of lower root and floor of bigger root, but edge cases are different
try:
    lower = int((T - (T**2 - 4 * d) ** 0.5) / 2) + 1
    upper = math.ceil(((T + (T**2 - 4 * d) ** 0.5) / 2) - 1)
    answer = upper - lower + 1
except TypeError:
    # 0 roots case
    answer = 0


# answer is number of numbers between lower and upper including them
print("The error in the margin of winning is: ", answer)
