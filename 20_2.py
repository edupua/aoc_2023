from pathlib import Path
from pprint import pprint
from collections import deque, defaultdict
import numpy

path = Path(__file__).parent / "../input/20"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()


# make a graph like dictionary for neighbours
graph = {}
# make a dict for modules and their "states"
modules = {}

for line in lines:
    module_type, nei_str = line[0], line[1:]
    root, neis = nei_str.split(" -> ")
    nei_list = neis.split(", ")

    graph.update({root: nei_list})

    if module_type == "%":
        modules.update({root: 0})
    elif module_type == "&":
        modules.update({root: {}})  # dict of roots connected and signals sent


# update conjunction modules
for root, neis in graph.items():
    for nei in neis:
        if nei in modules.keys() and isinstance(modules[nei], dict):
            modules[nei].update({root: 0})


def cal_pulse(root: str, pulse: int, is_conj: bool) -> int:
    if is_conj:
        for flow in modules[root].values():
            if flow == 0:
                return 1
        return 0

    if pulse == 1:
        return -1

    if modules[root] == 1:
        modules[root] = 0
        return 0
    elif modules[root] == 0:
        modules[root] = 1
        return 1
    else:
        print("skill issues")
        return -1


# for part 2 i found the root of 'rx' in my input it is 'cl' which is a conj: thus finding
# the frequency of all 'cl' roots being 'high' gives 'cl' low, and rx gets low, and lcm is the answer
rx_roots = []
for key, value in graph.items():
    if "rx" in value:
        rx_roots.append(key)

cl_roots = set([(key, root) for root in rx_roots for key in modules[root].keys()])
cl_iters = {}  # make dictionary for frequencies


def iteration(iter_no):
    signal_q = deque([("roadcaster", 0)])

    low_count = 1
    high_count = 0
    while signal_q:
        root, pulse = signal_q.popleft()
        if root not in graph.keys():
            continue

        for nei in graph[root]:
            # update pulse for neighbours in conj module
            if nei in modules.keys() and isinstance(modules[nei], dict):
                modules[nei][root] = pulse

            for key, parent in cl_roots:
                if modules[parent][key] == 1 and key not in cl_iters.keys():
                    cl_iters.update({key: iter_no})

            if nei in modules.keys():
                next_pulse = cal_pulse(nei, pulse, isinstance(modules[nei], dict))

            if next_pulse != -1:
                signal_q.append((nei, next_pulse))


for i in range(10000):
    iteration(i + 1)

print(
    "The minimum number of button presses  to get low in 'rx' is:",
    numpy.lcm.reduce(list(cl_iters.values())),
)
