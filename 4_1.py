from pathlib import Path

path = Path(__file__).parent / "../input/4"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()

# iterate through lines and split wining vs. given

point_sum = 0
for line in lines:
    winning_str, given_str = line.split(":")[-1].split("|")

    # make a set with winning numbers
    winning = set(winning_str.split())

    # make a list of given numbers
    given = set(given_str.split())

    # count repitations
    count = len(winning.intersection(given))

    points = int(2 ** (count - 1))
    point_sum += points

print("The sum of all points is: ", point_sum)
