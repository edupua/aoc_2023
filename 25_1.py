# christmas; and i have a life, i hate network flows. So import solution print(answer) for this one

from networkx import Graph, connected_components, minimum_edge_cut
from pathlib import Path
from pprint import pprint

path = Path(__file__).parent / "../input/25"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()


graph = Graph()
for line in lines:
    root, nei = line.split(": ")
    nei_list = nei.split()
    graph.add_node(root)
    for nei in nei_list:
        graph.add_node(nei)
        graph.add_edge(root, nei)

for a, b in minimum_edge_cut(graph):
    graph.remove_edge(a, b)

componentA, componentB = connected_components(graph)
product = len(componentA) * len(componentB)

print("The product of edges after seperating them into two components are:", product)
