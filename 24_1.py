from pathlib import Path
import math
from sympy import symbols, Eq, solve

path = Path(__file__).parent / "../input/24"

# open the input file
with open(path, "r") as file:
    lines = file.read().splitlines()
n = len(lines)

# finding intersection point using - Sympy code
x, y, x1, y1, x2, y2, x3, y3, x4, y4 = symbols("x y x1 y1 x2 y2 x3 y3 x4 y4")
equation1 = Eq((y - y1) / (y2 - y1), (x - x1) / (x2 - x1))
equation2 = Eq((y - y3) / (y4 - y3), (x - x3) / (x4 - x3))
answer = solve((equation1, equation2), (x, y), dict=True)
exp1 = answer[0][x]
exp2 = answer[0][y]
# print(exp1)
# print(exp2)

limit_s = 200000000000000
limit_f = 400000000000000

data = []
for line in lines:
    pos, vec = line.split(" @ ")
    p_x, p_y, _ = pos.split(", ")
    v_x, v_y, _ = vec.split(", ")
    p_x, p_y, v_x, v_y = int(p_x), int(p_y), int(v_x), int(v_y)

    if p_x > limit_f:
        if v_x >= 0:
            continue
        t = math.ceil((limit_f - p_x) / v_x)
        p_x = p_x + v_x * t
        p_y = p_y + v_y * t
    elif p_x < limit_s:
        if v_x <= 0:
            continue
        t = math.ceil((limit_s - p_x) / v_x)
        p_x = p_x + v_x * t
        p_y = p_y + v_y * t
    if p_y > limit_f:
        if v_y >= 0:
            continue
        t = math.ceil((limit_f - p_y) / v_y)
        p_x = p_x + v_x * t
        p_y = p_y + v_y * t
    elif p_y < limit_s:
        if v_y <= 0:
            continue
        t = math.ceil((limit_s - p_y) / v_y)
        p_x = p_x + v_x * t
        p_y = p_y + v_y * t

    if p_x > limit_f or p_x < limit_s or p_y > limit_f or p_y < limit_s:
        continue

    if v_x > 0:
        tx = (limit_f - p_x) // v_x
    else:
        tx = (limit_s - p_x) // v_x

    if v_y > 0:
        ty = (limit_f - p_y) // v_y
    else:
        ty = (limit_s - p_y) // v_y

    t = min(tx, ty)
    e_x = p_x + t * v_x
    e_y = p_y + t * v_y

    data.append(((p_x, e_x), (p_y, e_y)))


def intersection(a, b):
    (x1, x2), (y1, y2) = a
    (x3, x4), (y3, y4) = b

    ans1 = (y3 - y1) / (y2 - y1) - (x3 - x1) / (x2 - x1)
    ans2 = (y4 - y1) / (y2 - y1) - (x4 - x1) / (x2 - x1)
    ans3 = (y1 - y3) / (y4 - y3) - (x1 - x3) / (x4 - x3)
    ans4 = (y2 - y3) / (y4 - y3) - (x2 - x3) / (x4 - x3)

    if ans1 * ans2 > 0:
        return False
    if ans3 * ans4 > 0:
        return False

    # used sympy to find this
    Xt = (
        x1 * x3 * y2
        - x1 * x3 * y4
        - x1 * x4 * y2
        + x1 * x4 * y3
        - x2 * x3 * y1
        + x2 * x3 * y4
        + x2 * x4 * y1
        - x2 * x4 * y3
    ) / (x1 * y3 - x1 * y4 - x2 * y3 + x2 * y4 - x3 * y1 + x3 * y2 + x4 * y1 - x4 * y2)

    Yt = (
        x1 * y2 * y3
        - x1 * y2 * y4
        - x2 * y1 * y3
        + x2 * y1 * y4
        - x3 * y1 * y4
        + x3 * y2 * y4
        + x4 * y1 * y3
        - x4 * y2 * y3
    ) / (x1 * y3 - x1 * y4 - x2 * y3 + x2 * y4 - x3 * y1 + x3 * y2 + x4 * y1 - x4 * y2)

    (x1, x2), (y1, y2) = sorted(list(a[0])), sorted(list(a[1]))
    (x3, x4), (y3, y4) = sorted(list(b[0])), sorted(list(b[1]))
    if answer:
        if x1 <= Xt <= x2 and x3 <= Xt <= x4 and y1 <= Yt <= y2 and y3 <= Yt <= y4:
            return True
    return False


count = 0
for i in range(n):
    for j in range(i + 1, n):
        if intersection(data[i], data[j]):
            count += 1

print('The number of hailstones that "collide" are:', count)
