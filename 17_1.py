from pathlib import Path
from heapq import heapify, heappush, heappop

path = Path(__file__).parent / "../input/17"

# open the input file
with open(path, "r") as file:
    lines = file.read().split()
grid = [[int(char) for char in line] for line in lines]
n = len(grid)
m = len(grid[0])

dir_list = [(-1, 0), (0, -1), (0, 1), (1, 0)]
vis_edge = set()

# dijkstra idc its diextra but store the path length for each edge upto 3 most recent

# heap contains a tuple of ( cost, ( (s_i,s_j), (e_i,e_j), length ) ) like a priority, task tuple
mh = [(0, ((0, 0), (1, 0), 1)), (0, ((0, 0), (0, 1), 1))]
heapify(mh)

while mh:
    edge = heappop(mh)
    cost, data = edge
    start, end, length = data

    s_i, s_j = start
    e_i, e_j = end
    back = (s_i - e_i, s_j - e_j)
    front = (e_i - s_i, e_j - s_j)

    if e_i == n - 1 and e_j == m - 1:
        break

    cost += grid[e_i][e_j]

    for di, dj in dir_list:
        new_length = 1
        if (di, dj) == back:
            continue

        # can't go straight
        if (di, dj) == front:
            if length == 3:
                continue
            new_length = length + 1

        I = e_i + di
        J = e_j + dj

        if not (0 <= I < n and 0 <= J < m):
            continue

        edge = ((e_i, e_j), (I, J), new_length)
        if not edge in vis_edge:
            vis_edge.add(edge)
            heappush(mh, (cost, edge))

print("The minimum route takes cost: ", cost + grid[n - 1][m - 1])
