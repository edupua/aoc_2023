from email.policy import default
from pathlib import Path

path = Path(__file__).parent / "../input/13"

# open the input file
with open(path, "r") as file:
    blocks = file.read().split("\n\n")

summary = 0
for block in blocks:
    hblock = block.splitlines()
    n = len(hblock)
    m = len(hblock[0])

    vblock = ["".join(tuple) for tuple in list(zip(*hblock))]

    # now use manacher

    # for h block

    # print()
    # print()
    # pprint(hblock)
    p = {}  # p stores the max palindrome lengths at each center
    center, border = (0, 1), 1

    for index in range(n - 1):
        i = (index, index + 1)
        p.update({i: 0})
        avg = index + 0.5
        mirror = tuple(sorted((2 * center[1] - i[1], 2 * center[0] - i[0])))

        if avg < border and mirror in p.keys():
            p[i] = min(int(border - avg), p[mirror])

        # now expand from currrent limit
        while (
            i[1] + p[i] < n
            and i[0] - p[i] >= 0
            and hblock[i[1] + p[i]] == hblock[i[0] - p[i]]
        ):
            p[i] += 1

        border = max(i[1] + p[i], border)
        if border == i[1] + p[i]:
            center = i

    # pprint(p)
    valid_centers = [
        key[1] for key, value in p.items() if key[0] == value - 1 or n - key[1] == value
    ]
    # print(valid_centers)
    h_sum = sum(valid_centers)

    # for v block
    # print()
    # pprint(vblock)
    p = {}  # p stores the max palindrome lengths at each center
    center, border = (0, 1), 1

    for index in range(m - 1):
        i = (index, index + 1)
        p.update({i: 0})
        avg = index + 0.5
        mirror = tuple(sorted((2 * center[1] - i[1], 2 * center[0] - i[0])))

        if avg < border and mirror in p.keys():
            p[i] = min(int(border - avg), p[mirror])

        # now expand from currrent limit
        while (
            i[1] + p[i] < m
            and i[0] - p[i] >= 0
            and vblock[i[1] + p[i]] == vblock[i[0] - p[i]]
        ):
            p[i] += 1

        border = max(i[1] + p[i], border)
        if border == i[1] + p[i]:
            center = i

    # pprint(p)
    valid_centers = [
        key[1] for key, value in p.items() if key[0] == value - 1 or m - key[1] == value
    ]
    # print(valid_centers)
    v_sum = sum(valid_centers)

    summary += h_sum * 100 + v_sum
    # print(h_sum*100 + v_sum)

print("The sum of all the mirror counts is: ", summary)
