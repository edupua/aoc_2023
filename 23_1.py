from pathlib import Path
import sys

sys.setrecursionlimit(19881)

path = Path(__file__).parent / "../input/23"

# open the input file
with open(path, "r") as file:
    lines = file.read().split()
grid = [list(line) for line in lines]
n = len(grid)
m = len(grid[0])

shift = set([(-1, 0), (0, -1), (0, 1), (1, 0)])


def get_neighbours(node):
    i, j = node
    bad_set = set()
    for di, dj in shift:
        if not (0 <= i + di < n and 0 <= j + dj < m) or grid[i + di][j + dj] == "#":
            bad_set.add((di, dj))
        elif di == 0 and dj == -1 and grid[i + di][j + dj] == ">":
            bad_set.add((di, dj))
        elif di == -1 and dj == 0 and grid[i + di][j + dj] == "v":
            bad_set.add((di, dj))
    return [(i + di, j + dj) for di, dj in shift - bad_set]


max_path = 0


def dfs(node, path):
    global max_path
    path.add(node)
    if node[0] == n - 1:
        max_path = max(max_path, len(path))
    else:
        for nei in get_neighbours(node):
            if nei not in path:
                dfs(nei, path)
    path.remove(node)


start = (0, grid[0].index("."))
dfs(start, set())

print("The length of the longest hike is:", max_path - 1)
