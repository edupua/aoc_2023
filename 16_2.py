from pathlib import Path
from pprint import pprint
#from functools import cache
import sys

# could have used stacks but whatever this is the hack
sys.setrecursionlimit(12100)

path = Path(__file__).parent / "../input/16"

# open the input file
with open(path, "r") as file:
    lines = file.read().replace('\\','b').splitlines()
    grid = [ list(line) for line in lines ]

n = len(grid)
m = len(grid[0])

#pprint(grid)

def rec(i,j, direction, vis_grid, vis):
    
    if (i,j,direction) in vis or not ( 0 <= i < n and 0 <= j < m):
        return
    vis.add((i,j,direction))

    vis_grid[i][j] = '#'

    match direction:
        case 1:
            match grid[i][j]:
                case '.' | '-':
                    rec(i, j+1, 1, vis_grid,vis)
                case '|':
                    rec(i-1, j, 4, vis_grid,vis)
                    rec(i+1, j, 2, vis_grid,vis)
                case 'b':
                    rec(i+1, j, 2, vis_grid,vis)
                case '/':
                    rec(i-1, j, 4, vis_grid,vis)
                case _:
                    print("skill isseu 1")
        case 2:
            match grid[i][j]:
                case '.' | '|':
                    rec(i+1,j,2, vis_grid,vis)
                case '-':
                    rec(i,j-1,3, vis_grid,vis)
                    rec(i,j+1,1, vis_grid,vis)
                case 'b':
                    rec(i,j+1,1, vis_grid,vis)
                case '/':
                    rec(i,j-1,3, vis_grid,vis)
                case _:
                    print("skill isseu 2")
        case 3:
            match grid[i][j]:
                case '.' | '-':
                    rec(i,j-1,3, vis_grid,vis)
                case '|':
                    rec(i-1,j,4, vis_grid,vis)
                    rec(i+1,j,2, vis_grid,vis)
                case 'b':
                    rec(i-1,j,4, vis_grid,vis)
                case '/':
                    rec(i+1,j,2, vis_grid,vis)
                case _:
                    print("skill isseu 3")
        case 4:
            match grid[i][j]:
                case '.' | '|':
                    rec(i-1,j,4, vis_grid,vis)
                case '-':
                    rec(i,j-1,3, vis_grid,vis)
                    rec(i,j+1,1, vis_grid,vis)
                case 'b':
                    rec(i,j-1,3, vis_grid,vis)
                case '/':
                    rec(i,j+1,1, vis_grid,vis)
                case _:
                    print("skill isseu 4")
        case _:
            print("mega skill issue")
    
    return 

def return_count (i,j,direction):
    vis_grid = [ ['.']*m for _ in range(n) ] 
    vis = set()
    rec(i,j,direction,vis_grid,vis)
    return sum( line.count('#') for line in vis_grid )

max_tiles = 0
for i in range(n):
    max_tiles = max(max_tiles, return_count(i,0,1), return_count(i,m-1,3) )
for j in range(m):
    max_tiles = max(max_tiles, return_count(0,j,2), return_count(n-1,j,4) )
print("The maximum number of tiles that can be energised are", max_tiles )
